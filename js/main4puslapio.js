window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "Duobių išmušta"
      },
      data: [
      {
        type: "bar",
        dataPoints: [
        { y: 198, label: "Žirmūnai"},
        { y: 201, label: "Fabai"},
        { y: 202, label: "Naujamiestis"},
        { y: 500, label: "Žemieji paneriai"},
        { y: 395, label: "Pašilaičiai"},
        { y: 957, label: "Karoliniškės"}
        ]
      },
      {
        type: "bar",
        dataPoints: [
        { y: 166, label: "Žirmūnai"},
        { y: 144, label: "Fabai"},
        { y: 223, label: "Naujamiestis"},
        { y: 272, label: "Žemieji paneriai"},
        { y: 319, label: "Pašilaičiai"},
        { y: 759, label: "Karoliniškės"}
        ]
      },
      {
        type: "bar",
        dataPoints: [
        { y: 185, label: "Žirmūnai"},
        { y: 128, label: "Fabai"},
        { y: 246, label: "Naujamiestis"},
        { y: 272, label: "Žemieji paneriai"},
        { y: 296, label: "Pašilaičiai"},
        { y: 666, label: "Karoliniškės"}
        ]
      }
      ]
    });

chart.render();
    var chart2 = new CanvasJS.Chart("chart",
    {
      title:{
        text: "2017 m kebebų suvalgyta"
      },
      axisY: {
        title: "Kebabų kiekis",
        maximum: 1010
      },
      data: [
      {
        type: "bar",
        showInLegend: true,
        legendText: "Didelis",
        color: "gold",
        dataPoints: [
        { y: 198, label: "Kirtimai"},
        { y: 201, label: "Naujamiestis"},
        { y: 202, label: "Žirmūnai"},
        { y: 236, label: "Fabai"},
        { y: 395, label: "Stoties rajonas"},
        { y: 957, label: "Naujininkai"}
        ]
      },
      {
        type: "bar",
        showInLegend: true,
        legendText: "vidutinis",
        color: "silver",
        dataPoints: [
        { y: 166, label: "Kirtimai"},
        { y: 144, label: "Naujamiestis"},
        { y: 223, label: "Žirmūnai"},
        { y: 272, label: "Fabai"},
        { y: 319, label: "Stoties rajonas"},
        { y: 759, label: "Naujininkai"}
        ]
      },
      {
        type: "bar",
        showInLegend: true,
        legendText: "Mažas",
        color: "#DCA978",
        dataPoints: [
        { y: 185, label: "Kirtimai"},
        { y: 128, label: "Naujamiestis"},
        { y: 246, label: "Žirmūnai"},
        { y: 272, label: "Fabai"},
        { y: 296, label: "Stoties rajonas"},
        { y: 666, label: "Naujininkai"}
        ]
      }
      ]
    });

chart2.render();
}